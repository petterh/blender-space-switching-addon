import bpy

def set_active_object(obj, deselect_all=False):
    if deselect_all:
        bpy.ops.object.select_all(action='DESELECT')

    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj

def unselect_active_object():
    bpy.context.active_object.select_set(False)

def get_pose_bone(armature_object, pose_bone_name):
    pose_bone = armature_object.pose.bones.get(pose_bone_name)
    if pose_bone is None:
        print("WARNING: Cannot find pose bone " + pose_bone_name + " in armature " + armature_object.name)
        return None

    return pose_bone
