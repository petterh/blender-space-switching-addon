from ..aim_space import aim_space_utils
from ..world_space import world_space_utils
from ..child_of import child_of_utils

def is_space_switched(pose_bone):
    return world_space_utils.is_worldspace_driven(pose_bone) or child_of_utils.is_child_of_driven(pose_bone) or aim_space_utils.is_pose_bone_aimspace_driven(pose_bone)
