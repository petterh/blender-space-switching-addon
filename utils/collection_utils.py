import bpy


def collection_exist(collection_name):
    """ Check if there is a collection with the supplied name."""
    return collection_name in bpy.data.collections


def get_collection(collection_name):
    """Get collection by name. If none is found return None"""
    if collection_exist(collection_name):
        return bpy.data.collections[collection_name]
    else:
        return None


def create_collection(context, collection_name, parent_collection=None, active=False):
    """Create collection. If it already exists, return the existing collection."""
    if collection_exist(collection_name):
        return bpy.data.collections[collection_name]

    collection = bpy.data.collections.new(collection_name)

    # If no parent collection is defined add to scene
    if parent_collection is None:
        context.scene.collection.children.link(collection)
    else:
        parent_collection.children.link(collection)

    if active:
        set_collection_active(collection_name)

    return collection


def get_collection_name(prefix):
    """ Get next available/non-existing collection name, starting with supplied prefix.
     If non found before 1000, return just the prefix and let Blender solve it."""
    if collection_exist(prefix) is False:
        return prefix

    for i in range(2, 1000):
        uv_name = prefix + " " + str(i)

        if collection_exist(uv_name) is False:
            return uv_name

    # Fallback to just returning prefix and let Blender increment name
    return prefix


def remove_object_from_collect(obj, collection_name):
    """Remove/Unlink object from collection."""
    collection = get_collection(collection_name)
    collection.objects.unlink(obj)


def add_to_collection(objects, collection_name):
    """Add the objects to the supplied collection name."""
    collection = get_collection(collection_name)
    if collection is None:
        return

    for obj in objects[:]:
        if obj.name in collection.objects:
            continue

        collection.objects.link(obj)


def set_collection_active(collection_name):
    """Set the supplied collection name to be the active one"""
    layer_collection = _get_collection_layer(
        bpy.context.view_layer.layer_collection, collection_name)
    bpy.context.view_layer.active_layer_collection = layer_collection


def _get_collection_layer(layer_collection, collection_name):
    found = None
    if layer_collection.name == collection_name:
        return layer_collection
    for layer in layer_collection.children:
        found = _get_collection_layer(layer, collection_name)
        if found:
            return found
