def create_constraint(constraint_type, target, constraint_name, object_to_apply_constraint_to, sub_target_name=None, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    constraint = object_to_apply_constraint_to.constraints.new(constraint_type)
    constraint.target = target
    if sub_target_name is not None:
        constraint.subtarget = sub_target_name
    constraint.name = constraint_name


def create_transform_constraint(target, constraint_name, object_to_apply_constraint_to, sub_target_name=None, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    ws_constraint = object_to_apply_constraint_to.constraints.new(
        'COPY_TRANSFORMS')
    ws_constraint.target = target
    if sub_target_name is not None:
        ws_constraint.subtarget = sub_target_name
    ws_constraint.name = constraint_name

    return ws_constraint


def create_rotation_constraint(target, constraint_name, object_to_apply_constraint_to, sub_target_name=None, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    cp_constraint = object_to_apply_constraint_to.constraints.new(
        'COPY_ROTATION')
    cp_constraint.target = target
    if sub_target_name is not None:
        cp_constraint.subtarget = sub_target_name
    cp_constraint.name = constraint_name


def create_location_constraint(target, constraint_name, object_to_apply_constraint_to,
                               sub_target_name=None, head_or_tail=0, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    cp_constraint = object_to_apply_constraint_to.constraints.new(
        'COPY_LOCATION')
    cp_constraint.target = target
    if sub_target_name is not None:
        cp_constraint.subtarget = sub_target_name
        cp_constraint.head_tail = head_or_tail
    cp_constraint.name = constraint_name
    return cp_constraint


def create_scale_constraint(target, constraint_name, object_to_apply_constraint_to,
                            sub_target_name=None, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    cp_constraint = object_to_apply_constraint_to.constraints.new('COPY_SCALE')
    cp_constraint.target = target
    if sub_target_name is not None:
        cp_constraint.subtarget = sub_target_name
    cp_constraint.name = constraint_name


def create_child_of_constraint(target, constraint_name, object_to_apply_constraint_to,
                               sub_target_name, remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    constraint = object_to_apply_constraint_to.constraints.new('CHILD_OF')
    constraint.target = target
    if sub_target_name is not None:
        constraint.subtarget = sub_target_name
    constraint.name = constraint_name

    return constraint


def create_damped_track_constraint(target, constraint_name, object_to_apply_constraint_to,
                                   sub_target_name, track_axis='y', remove_existing=True):
    if remove_existing:
        _remove_existing(object_to_apply_constraint_to, constraint_name)

    constraint = object_to_apply_constraint_to.constraints.new('DAMPED_TRACK')
    constraint.target = target
    if sub_target_name is not None:
        constraint.subtarget = sub_target_name
    constraint.track_axis = _get_track_axis_from_string(track_axis)
    constraint.name = constraint_name
    return constraint


def remove_constraint_with_name(constrained_object, constraint_name):
    for constraint in constrained_object.constraints:
        if constraint.name.startswith(constraint_name):
            constrained_object.constraints.remove(constraint)


def has_constraint_with_name(constrained_object, constraint_name):
    if constrained_object.constraints is not None:
        for constraint in constrained_object.constraints:
            if constraint.name.startswith(constraint_name):
                return True
        return False
    return False


def _remove_existing(constrained_object, constraint_name):
    if has_constraint_with_name(constrained_object, constraint_name):
        remove_constraint_with_name(constrained_object, constraint_name)


def _get_track_axis_from_string(track_axis_string):
    if track_axis_string == 'x':
        return 'TRACK_X'
    if track_axis_string == '-x':
        return 'TRACK_NEGATIVE_X'
    if track_axis_string == 'y':
        return 'TRACK_Y'
    if track_axis_string == '-y':
        return 'TRACK_NEGATIVE_Y'
    if track_axis_string == 'z':
        return 'TRACK_Z'
    if track_axis_string == '-z':
        return 'TRACK_NEGATIVE_Z'
