def get_attribute(attribute_object, attribute_name):
    """Make sure we return something even though the attribute doesn't exist, so that we can build logic upon always return atleast None"""
    if hasattr(attribute_object, attribute_name):
        return getattr(attribute_object, attribute_name, None)
    else:
        return None


def set_attribute(attribute_object, attribute_name, attribute_value):
    setattr(attribute_object, attribute_name, attribute_value)


def has_attribute(attribute_object, attribute_name):
    return hasattr(attribute_object, attribute_name)


def clear_attribute_if_it_exists(attribute_object, attribute_name):
    if hasattr(attribute_object, attribute_name):
        set_attribute(attribute_object, attribute_name, None)
