import bpy
from .. import constants
from .. utils import attrib_utils
from .. bones import bones
from .. child_of import child_of_utils
from .. center_of_mass import center_of_mass_utils
from .. center_of_mass import center_of_mass_constants


def create_driver_empty_object(context, object_name_suffix, driven_armature, driven_pose_bone, in_front=False):
    bpy.ops.object.empty_add(type='PLAIN_AXES')
    new_object = context.active_object
    new_object.name = driven_pose_bone.name + object_name_suffix
    new_object.show_in_front = in_front
    _set_driven_pose_bone_property(
        new_object, driven_armature, driven_pose_bone.name)

    return new_object


def get_pose_bone_driver_objects(context, pose_bone_name):
    return [obj for obj in context.scene.objects if attrib_utils.get_attribute(obj, constants.DRIVEN_BONE_PROPERTY_NAME) == pose_bone_name]


def get_valid_driver_objects(object_list):
    valid_driver_objects = []
    for driver_object in object_list[:]:
        if _is_valid_driver_object(driver_object):
            valid_driver_objects.append(driver_object)

        if center_of_mass_utils.is_center_of_mass_root(driver_object):
            for com_child in driver_object.children[:]:
                if _is_valid_driver_object(com_child):
                    valid_driver_objects.append(com_child)

    return valid_driver_objects


def _is_valid_driver_object(obj):
    if attrib_utils.get_attribute(
            obj, constants.DRIVEN_ARMATURE_OBJECT) is None:
        return False
    if attrib_utils.get_attribute(
            obj, constants.DRIVEN_BONE_PROPERTY_NAME) is None:
        return False

    return True


def get_armature_objects(driver_objects):
    armature_objects = []

    for driver_object in driver_objects[:]:
        armature_object = attrib_utils.get_attribute(
            driver_object, constants.DRIVEN_ARMATURE_OBJECT)

        if armature_object is not None and armature_object.type == 'ARMATURE':
            if armature_object in armature_objects:
                continue

            armature_objects.append(armature_object)

    return armature_objects


def get_related_driven_bones(driven_armature, driver_object_names):
    driver_objects = []
    driven_bone_names = []

    for driver_object_name in driver_object_names[:]:
        driver_object = bpy.context.scene.objects.get(driver_object_name)
        if driver_object and driver_object not in driver_objects:
            driver_objects.append(driver_object)

    driver_objects_to_process = driver_objects.copy()
    for driver_object in driver_objects_to_process[:]:

        if attrib_utils.get_attribute(driver_object, constants.DRIVEN_ARMATURE_OBJECT) == driven_armature:

            driven_bone_name = attrib_utils.get_attribute(
                driver_object, constants.DRIVEN_BONE_PROPERTY_NAME)

            if driven_bone_name is not None:
                driven_bone_names.append(driven_bone_name)

    return driven_bone_names


def delete_driver_objects(context, report):
    pose_bones = context.selected_pose_bones.copy()
    armature_object = context.active_object
    armature = armature_object.data

    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

    for pose_bone in pose_bones[:]:
        space_switched_bone = bones.SpaceSwitchedPoseBone(
            context, report, pose_bone)

        space_switched_bone.clear_constraints()
        space_switched_bone.clear_references()
        _delete_connected_driver_objects(context, space_switched_bone.name)
        _delete_existing_child_of_keyframes(armature_object, pose_bone.name)

    if center_of_mass_utils.is_armature_driven_by_center_of_mass(armature):
        com_root_object = attrib_utils.get_attribute(
            armature, center_of_mass_constants.ROOT_REF_PROP_NAME)

        if len(com_root_object.children) < 1:
            _delete_driver_object(com_root_object)


def _set_driven_pose_bone_property(driver_object, driven_armature, pose_bone_name):
    attrib_utils.set_attribute(
        driver_object, constants.DRIVEN_ARMATURE_OBJECT, driven_armature)
    attrib_utils.set_attribute(
        driver_object, constants.DRIVEN_BONE_PROPERTY_NAME, pose_bone_name)


def _delete_existing_child_of_keyframes(armature, bone_name):
    # Remove keyframes related to child of constraint.
    fcurves = armature.animation_data.action.fcurves
    fcurves_to_remove = []
    for fcurve in fcurves[:]:
        data_path = fcurve.data_path
        if data_path.find(bone_name) >= 0:
            if data_path.find(child_of_utils.CHILD_OF_CONSTRAINT_NAME) >= 0:
                fcurves_to_remove.append(fcurve)

    for fcurve in fcurves_to_remove[:]:
        fcurves.remove(fcurve)

    fcurves_to_remove = []


def _delete_connected_driver_objects(context, pose_bone_name):
    driver_objects = get_pose_bone_driver_objects(
        context, pose_bone_name)

    for driver_object in driver_objects[:]:
        _delete_driver_object(driver_object)


def _delete_driver_object(driver_object):
    temp_ref_to_driver_action = None
    driver_object.hide_viewport = False

    # Check if the driver object has an action linked and if so, save reference to it
    if driver_object.animation_data and driver_object.animation_data.action:
        temp_ref_to_driver_action = driver_object.animation_data.action

    # Have the object active
    bpy.ops.object.select_all(action='DESELECT')
    driver_object.select_set(True)
    driver_object.animation_data_clear()
    bpy.data.objects.remove(driver_object, do_unlink=True)

    # Delete action after object has been deleted
    if temp_ref_to_driver_action is not None:
        bpy.data.actions.remove(temp_ref_to_driver_action)
