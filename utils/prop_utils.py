import bpy
from . import attrib_utils

def register_string_property(prop_name, prop_description, owner_type, prop_attrib_name):
    string_prop = bpy.props.StringProperty(
        name=prop_name,
        description=prop_description
    )
    attrib_utils.set_attribute(owner_type, prop_attrib_name,
                        string_prop)

def register_float_property(prop_name, prop_description, owner_type, prop_attrib_name):
    float_prop = bpy.props.FloatProperty(
        name=prop_name,
        description=prop_description
    )
    attrib_utils.set_attribute(owner_type, prop_attrib_name,
                        float_prop)

def register_pointer_property(prop_name, prop_description, prop_type, owner_type, prop_attrib_name):
    pointer_prop = bpy.props.PointerProperty(
        type=prop_type,
        name=prop_name,
        description=prop_description
    )
    attrib_utils.set_attribute(owner_type, prop_attrib_name,
                        pointer_prop)

def register_collection_property(prop_name, prop_description, prop_type, owner_type, prop_attrib_name):
    coll_prop = bpy.props.CollectionProperty(
        type=prop_type,
        name=prop_name,
        description=prop_description
    )
    attrib_utils.set_attribute(owner_type, prop_attrib_name,
                        coll_prop)
