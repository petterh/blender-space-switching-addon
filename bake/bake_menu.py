from .. utils import menu_utils
from .bake_operator import PHSPACESWITCHING_OT_bake_driven_bone

def create_bake_menu_items(layout):
    menu_utils.create_menu_option(
        layout, PHSPACESWITCHING_OT_bake_driven_bone)
