import bpy
from .. utils import driver_utils
from .. utils import blender_utils
from .. import constants

# pylint: disable=C0103


class PHSPACESWITCHING_OT_bake_driven_bone(bpy.types.Operator):
    """Bake driven bones"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + \
        ".bake_driven_pose_bones"
    bl_label = "Bake driven pose bones"
    bl_options = {'REGISTER', 'UNDO'}

    clean_up: bpy.props.BoolProperty(
        name="Clean up",
        description="Cleanup driver objects and actions after bake",
        default=True
    )

    clean_curves: bpy.props.BoolProperty(
        name="Clean curves",
        description="After baking curves, remove redundant keys",
        default=True
    )

    use_playback_range: bpy.props.BoolProperty(
        name="Use playback range",
        description="Use the playback range when baking",
        default=True
    )

    start_frame: bpy.props.IntProperty(
        name="Start frame",
        description="The start frame to bake from",
        default=0
    )

    end_frame: bpy.props.IntProperty(
        name="End frame",
        description="Bake action up until this frame",
        default=24
    )

    overwrite_action: bpy.props.BoolProperty(
        name="Overwrite Action",
        description="Are we to overwrite the current action, or do we create a new one?",
        default=True
    )

    @classmethod
    def poll(cls, context):
        if context.active_object is None:
            return False

        if context.mode == 'OBJECT':
            if len(driver_utils.get_valid_driver_objects(context.selected_objects)) > 0:
                return True

        if context.mode == 'POSE':
            if len(context.selected_pose_bones) > 0 or context.active_pose_bone is not None:
                return True

        return False

    def execute(self, context):
        initial_mode = context.mode

        if context.mode == 'OBJECT':
            driver_objects = driver_utils.get_valid_driver_objects(
                context.selected_objects)

            # As driver objects are deleted during the process we use names instead.
            driver_object_names = []
            for driver_object in driver_objects[:]:
                driver_object_names.append(driver_object.name)

            driven_armatures_objects = driver_utils.get_armature_objects(
                driver_objects)

            for driven_armature_object in driven_armatures_objects[:]:
                self._select_driven_bones(
                    context, driven_armature_object, driver_object_names)

                self._bake_action(context, driven_armature_object)

        if context.mode == 'POSE':
            driven_armature_object = context.active_object
            pose_bones = context.selected_pose_bones.copy()

            # Unselect pose bones not driven
            for pose_bone in pose_bones[:]:
                driver_object = driver_utils.get_pose_bone_driver_objects(
                    context, pose_bone.name)

                if not driver_object or driver_object is None:
                    pose_bone.bone.select = False

            if len(context.selected_pose_bones) > 0:
                self._bake_action(context, driven_armature_object)

        bpy.ops.object.mode_set(mode=initial_mode, toggle=False)

        return {'FINISHED'}

    # pylint: disable=W0613
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()

        col.prop(self, "clean_up")
        col.prop(self, "clean_curves")
        col.prop(self, "overwrite_action")
        col.prop(self, "use_playback_range")

        if self.use_playback_range is False:
            col.label(text="Select start and end frame")
            row = col.row()
            row.prop(self, "start_frame")
            row.prop(self, "end_frame")

    def _select_matching_pose_bones(self, driver_object):
        # Find the pose bones

        pass

    def _bake_action(self, context, driven_armature_object):
        if len(context.selected_pose_bones) > 0:
            if self.use_playback_range:
                selected_start_frame = context.scene.frame_start
                selected_end_frame = context.scene.frame_end
            else:
                selected_start_frame = self.start_frame
                selected_end_frame = self.end_frame

            bpy.ops.nla.bake(
                frame_start=selected_start_frame,
                frame_end=selected_end_frame,
                step=1,
                only_selected=True,
                visual_keying=True,
                clear_constraints=False,
                clear_parents=False,
                clean_curves=self.clean_curves,
                use_current_action=self.overwrite_action,
                bake_types={'POSE'}
            )
            if self.clean_up:
                driver_utils.delete_driver_objects(
                    context, self.report)
        else:
            self.report(
                {'WARNING'}, 'Could not find any driven bones for ' + driven_armature_object.name)

    def _select_driven_bones(self, context, driven_armature, driver_object_names):
        driven_bones_cnt = 0
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        driven_bone_names = driver_utils.get_related_driven_bones(
            driven_armature, driver_object_names)

        bpy.ops.object.select_all(action='DESELECT')

        blender_utils.set_active_object(driven_armature)
        bpy.ops.object.mode_set(mode='POSE', toggle=False)
        bpy.ops.pose.select_all(action='DESELECT')

        for driven_bone_name in driven_bone_names[:]:
            pose_bone = driven_armature.data.bones.get(
                driven_bone_name)

            if pose_bone is not None:
                driven_bones_cnt += 1
                # A valid bone has been found, enable bake
                pose_bone.select = True

        return driven_bones_cnt
