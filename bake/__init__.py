import bpy
from .bake_operator import PHSPACESWITCHING_OT_bake_driven_bone


classes = [
    PHSPACESWITCHING_OT_bake_driven_bone
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
