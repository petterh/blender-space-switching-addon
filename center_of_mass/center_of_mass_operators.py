import bpy
from .. import constants
from .center_of_mass_driver import CenterOfMassRoot
from .center_of_mass_driver import CenterOfMassChild
from ..utils import blender_utils
from ..utils import attrib_utils
from . import center_of_mass_constants
from . import center_of_mass_properties


# pylint: disable=C0103


class PHSPACESWITCHING_OT_create_center_of_mass_object(bpy.types.Operator):
    """Create Center of Mass object for selected armature"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".create_center_of_mass_object"
    bl_label = "Create drivers"
    bl_options = {'REGISTER', 'UNDO'}

    bake: bpy.props.BoolProperty(
        name="Bake and create limb drivers",
        description="Bake action after creating and setting up the CoM object and setup drivers for all selected limb groups",
        default=True
    )

    cog: bpy.props.BoolProperty(
        name="COG",
        description="Include COG when setting up drivers and baking",
        default=True
    )

    feet: bpy.props.BoolProperty(
        name="Feet",
        description="Include feet when setting up drivers and baking",
        default=True
    )

    hands: bpy.props.BoolProperty(
        name="Hands",
        description="Include hands when setting up drivers and baking",
        default=True
    )

    other: bpy.props.BoolProperty(
        name="Other",
        description="Include other when setting up drivers and baking",
        default=True
    )

    @classmethod
    def poll(cls, context):
        if context.active_object is None:
            return False

        if context.mode == 'OBJECT' or context.mode == 'POSE':
            if context.active_object is not None and context.active_object.type == 'ARMATURE':
                if _has_root_object(context.active_object.data) is False:
                    return True

        return False

    def execute(self, context):
        initial_mode = context.mode
        armature_object = context.active_object

        # Check that the required weights and relevant bones are set
        weight_collection = attrib_utils.get_attribute(
            armature_object.data, center_of_mass_constants.COLLECTION_PROP_NAME)
        if len(weight_collection) < 1:
            self.report(
                {'WARNING'}, 'You need to create/set the center of mass key weights in the Object properties/Space Switching panel.')
            return {'CANCELLED'}
        for prop in weight_collection[:]:
            if prop.bone == 'none':
                self.report(
                    {'WARNING'}, 'Set all the corresponding bone for all center of mass key weights in the Object properties/Space Switching panel.')
                return {'CANCELLED'}

        if context.mode == 'POSE':
            bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        center_of_mass_root = CenterOfMassRoot(
            context, armature_object)

        if self.bake:
            self._create_children_and_bake(context, armature_object, center_of_mass_root)

            blender_utils.set_active_object(center_of_mass_root.object)
        else:
            center_of_mass_root.setup_constraints()

        if initial_mode == 'POSE' and self.bake is False:
            # If the mode was POSE we return to the previous selected state
            center_of_mass_root.unselect_driver_object()
            blender_utils.set_active_object(armature_object)
            bpy.ops.object.mode_set(mode=initial_mode, toggle=False)

        return {'FINISHED'}

    # pylint: disable=W0613
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()

        col.prop(self, "bake")
        if self.bake is True:
            col.label(text="Include")
            col.prop(self, "cog")
            col.prop(self, "feet")
            col.prop(self, "hands")
            col.prop(self, "other")

    def _create_center_of_mass_children(self, context, child_prop_collection, center_of_mass_root, armature_object):
        active_child_bones = []
        for child_bone_name in self._get_child_bone_names(child_prop_collection):
            child_bone = blender_utils.get_pose_bone(
                armature_object, child_bone_name)
            if child_bone is not None:
                com_child = CenterOfMassChild(
                    context, center_of_mass_root, armature_object, child_bone)
                active_child_bones.append(com_child)

        return active_child_bones

    def _get_child_bone_names(self, child_bone_collection):
        valid_child_bone_names = []
        for child_bone in child_bone_collection[:]:
            match child_bone.group.lower():
                case "cog":
                    if self.cog:
                        valid_child_bone_names.append(child_bone.bone_name)
                case "hands":
                    if self.hands:
                        valid_child_bone_names.append(child_bone.bone_name)
                case "feet":
                    if self.feet:
                        valid_child_bone_names.append(child_bone.bone_name)
                case "other":
                    if self.other:
                        valid_child_bone_names.append(child_bone.bone_name)

        return valid_child_bone_names

    def _create_children_and_bake(self, context, armature_object, center_of_mass_root):
        armature = armature_object.data
        com_child_props = attrib_utils.get_attribute(
        armature, center_of_mass_constants.CHILD_BONES_PROP_NAME)

        driven_com_children = self._create_center_of_mass_children(
            context, com_child_props, center_of_mass_root, armature_object)

        center_of_mass_root.setup_constraints()
        center_of_mass_root.bake_action(context)

        for driven_com_child in driven_com_children[:]:
            driven_com_child.bake_and_link_pose_bones(context)

def _has_root_object(armature):
    root_prop = attrib_utils.get_attribute(armature, center_of_mass_constants.ROOT_REF_PROP_NAME)

    if root_prop is None:
        return False

    return root_prop.name in bpy.context.scene.objects


class PHSPACESWITCHING_OT_add_to_center_of_mass_child_option(bpy.types.Operator):
    """Add bone to list of possible world space bones controlled by center of mass root"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + \
        ".add_to_center_of_mass_child_group"
    bl_label = "Add bone to a child group"
    bl_options = {'REGISTER', 'UNDO'}

    group: bpy.props.EnumProperty(name="Property group",
                                       description="The property group to which these are to belong to",
                                       items=center_of_mass_properties.get_groups())

    @classmethod
    def poll(cls, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            return False

        if context.mode != 'POSE':
            return False

        if len(context.selected_pose_bones) > 0:
            return True

        return False

    def execute(self, context):
        armature = context.active_object.data

        pose_bones = context.selected_pose_bones.copy()

        limb_collection = attrib_utils.get_attribute(
            armature, center_of_mass_constants.CHILD_BONES_PROP_NAME)

        for pose_bone in pose_bones[:]:
            bone_in_list = False
            for limb in limb_collection[:]:
                if pose_bone.name == limb.bone_name:
                    limb.group = str(self.group)
                    bone_in_list = True
                    continue
            if bone_in_list:
                continue

            new_limb = limb_collection.add()
            new_limb.bone_name = pose_bone.name
            new_limb.group = str(self.group)
        return {'FINISHED'}

    # pylint: disable=W0613
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)
