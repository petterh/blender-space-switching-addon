from ..constants import CONSTRAINT_PREFIX

COLLECTION_PROP_NAME = 'phss_center_of_mass_collection'
CHILD_BONES_PROP_NAME = 'phss_center_of_mass_children'
TOTAL_WEIGHT_PROP_NAME = 'phss_center_of_mass_total_weight'
ARMATURE_REF_PROP_NAME = 'phss_center_of_mass_armature'
ROOT_REF_PROP_NAME = 'phss_center_of_mass_object'
ACTION_NAME_PREFIX = 'CoM Action for '
OBJECT_NAME_SUFFIX = '-COM'
CONSTRAINT_NAME_PREFIX = 'Center of Mass - '
CONSTRAINT_NAME = CONSTRAINT_PREFIX + 'Center of Mass Constraint'
