import bpy
from .. utils import menu_utils
from .center_of_mass_operators import PHSPACESWITCHING_OT_create_center_of_mass_object
from .center_of_mass_operators import PHSPACESWITCHING_OT_add_to_center_of_mass_child_option


# pylint: disable=C0103
class VIEW3D_MT_phspaceswitching_center_of_mass_menu(bpy.types.Menu):
    bl_idname = "VIEW3D_MT_phspaceswitching_center_of_mass_menu"
    bl_label = 'Center of Mass (Experimental)'

    # pylint: disable=W0613
    def draw(self, context):
        layout = self.layout
        self._create_center_of_mass_menu_options(layout)

    def _create_center_of_mass_menu_options(self, layout):
        menu_utils.create_menu_option(
            layout, PHSPACESWITCHING_OT_create_center_of_mass_object)
        menu_utils.create_menu_option(
            layout, PHSPACESWITCHING_OT_add_to_center_of_mass_child_option)


def create_center_of_mass_menu(layout):
    layout.menu(
        VIEW3D_MT_phspaceswitching_center_of_mass_menu.bl_idname, text=VIEW3D_MT_phspaceswitching_center_of_mass_menu.bl_label)
