from ..utils import constraint_utils
from . import center_of_mass_constants
from ..utils import attrib_utils


def is_center_of_mass_driven(pose_bone):
    return constraint_utils.has_constraint_with_name(pose_bone, center_of_mass_constants.CONSTRAINT_NAME)


def is_center_of_mass_root(obj):
    center_of_pass_root_prop = attrib_utils.get_attribute(
        obj, center_of_mass_constants.ARMATURE_REF_PROP_NAME)

    return center_of_pass_root_prop is not None

def is_armature_driven_by_center_of_mass(armature):
    com_prop = attrib_utils.get_attribute(armature, center_of_mass_constants.ROOT_REF_PROP_NAME)

    return com_prop is not None
