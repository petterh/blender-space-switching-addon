import bpy
from .. import constants
from .. utils import attrib_utils
from . import center_of_mass_constants as com_constants
from . import center_of_mass_constants


class CreateDefaultCenterOfMassPropertiesOperator(bpy.types.Operator):

    bl_idname = constants.MODULE_IDNAME_PREFIX + '.com_props_create_default_props'
    bl_label = 'Create Center of Mass Default Props'

    def execute(self, context):
        default_properties = [
            ('Head', 7.3),
            ('Chest', 35.8),
            ('Abdomen', 10.1),
            ('Elbow Left', 4.8),
            ('Elbow Right', 4.8),
            ('Hand Left', 0.8),
            ('Hand Right', 0.8),
            ('Knee Left', 15.9),
            ('Knee Right', 15.9),
            ('Foot Left', 1.9),
            ('Foot Right', 1.9)
        ]

        if context.active_object is None or context.active_object.type != 'ARMATURE':
            self.report(
                'WARNING', 'No armature is selected. Unable to create Center of Mass properties')
            return {'CANCELLED'}

        com_collection = attrib_utils.get_attribute(
            context.active_object.data, com_constants.COLLECTION_PROP_NAME)

        for key_value_pair in default_properties:
            key_found = False
            if len(com_collection) > 0:
                for com_prop in com_collection[:]:
                    if com_prop.name == key_value_pair[0]:
                        com_prop.weight = key_value_pair[1]
                        key_found = True
                        continue

            if key_found is False:
                com_prop = com_collection.add()
                com_prop.name = key_value_pair[0]
                com_prop.weight = key_value_pair[1]

        return {'FINISHED'}


class AddCenterOfMassWeightPropertyOperator(bpy.types.Operator):

    bl_idname = constants.MODULE_IDNAME_PREFIX + '.com_props_add_prop'
    bl_label = 'Add Center of Mass prop'

    def execute(self, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            self.report(
                'WARNING', 'No armature is selected. Unable to create Center of Mass properties')
            return {'CANCELLED'}

        com_collection = attrib_utils.get_attribute(
            context.active_object.data, com_constants.COLLECTION_PROP_NAME)


        com_prop = com_collection.add()
        com_prop.name = '<Set name>'
        com_prop.weight = 0.0

        return {'FINISHED'}


class RemoveCenterOfMassControlledBoneOperator(bpy.types.Operator):

    bl_idname = constants.MODULE_IDNAME_PREFIX + '.com_remove_bone_from_controlled_group'
    bl_label = 'Remove control bone from the controlled group list'

    bone_name: bpy.props.StringProperty(
        name="Bone name",
        description="The bone name to remove from the list"
    )

    def execute(self, context):
        active_object = context.active_object

        if active_object.type != 'ARMATURE':
            self.report({'ERROR'}, 'Not run on an armature!')
            return {'CANCELLED'}

        armature = active_object.data

        bone_collection = attrib_utils.get_attribute(armature, center_of_mass_constants.CHILD_BONES_PROP_NAME)

        bone_index = 0
        bone_found = False
        for bone in bone_collection[:]:
            if bone.bone_name == self.bone_name:
                bone_found = True
                break
            bone_index += 1

        if bone_found:
            bone_collection.remove(bone_index)

        return {'FINISHED'}


class RemoveCenterOfMassWeightOperator(bpy.types.Operator):

    bl_idname = constants.MODULE_IDNAME_PREFIX + \
        '.com_remove_weight_from_weight_group'
    bl_label = 'Remove property from the weight group list'

    weight_name: bpy.props.StringProperty(
        name="Weight name",
        description="The weight name to remove from the list"
    )

    def execute(self, context):
        active_object = context.active_object

        if active_object.type != 'ARMATURE':
            self.report({'ERROR'}, 'Not run on an armature!')
            return {'CANCELLED'}

        armature = active_object.data

        weights_collection = attrib_utils.get_attribute(
            armature, center_of_mass_constants.COLLECTION_PROP_NAME)

        weight_index = 0
        weight_found = False
        for weight in weights_collection[:]:
            if weight.name == self.weight_name:
                weight_found = True
                break
            weight_index += 1

        if weight_found:
            weights_collection.remove(weight_index)

        return {'FINISHED'}


def create_center_of_mass_property_menu(armature, layout):
    collection = attrib_utils.get_attribute(
        armature, center_of_mass_constants.COLLECTION_PROP_NAME)
    total_weight = attrib_utils.get_attribute(
        armature, center_of_mass_constants.TOTAL_WEIGHT_PROP_NAME)
    box = layout.box()
    box.label(text="Center of Mass")

    row_factors = [0.15, 0.35, 0.3, 0.15]

    col = box.column(align=True)
    _create_column_headers(col, row_factors)

    for property_group in collection[:]:
        _create_property_row(
            col, property_group, total_weight, row_factors)

    _create_total_weight_row(col, total_weight, row_factors)
    _create_buttons(col, collection)


    com_children = attrib_utils.get_attribute(armature, center_of_mass_constants.CHILD_BONES_PROP_NAME)


    row_factors = [0.20, 0.10, 0.03]
    col.split(align=True)
    col.label(text="")
    col.label(text="Optionally controlled bones")
    row = col.split(factor=row_factors[0], align=True)
    row.label(text="Bone")
    row = row.split(factor=row_factors[1], align=True)
    row.label(text="Group")
    row = row.split(factor=row_factors[1], align=True)
    row.label(text="Delete")
    for com_child_prop in com_children[:]:
        row = col.split(factor=row_factors[0], align=True)
        row.label(text=com_child_prop.bone_name)
        row = row.split(factor=row_factors[1], align=True)
        row.label(text=com_child_prop.group)
        row = row.split(factor=row_factors[2], align=True)
        row.operator(
            RemoveCenterOfMassControlledBoneOperator.bl_idname, text="", icon='TRASH').bone_name = com_child_prop.bone_name


def _create_column_headers(column, row_factors):
    row = column.split(factor=row_factors[0], align=True)
    row.label(text="Name")
    row = row.split(factor=row_factors[1], align=True)
    row.label(text="Bone")
    row = row.split(factor=row_factors[2], align=True)
    row.label(text="Weight")
    row = row.split(factor=row_factors[3], align=True)
    row.label(text="%")


def _create_property_row(column, property_group, total_weight, row_factors):
    # Get this property weight in percent of the total weight
    # But only if the total_weight is defined
    if total_weight > 0:
        property_pct = property_group.weight / total_weight

    row = column.split(factor=row_factors[0], align=True)
    row.prop(property_group, "name", text="")
    row = row.split(factor=row_factors[1], align=True)
    row.prop(property_group, "bone", text="")
    row = row.split(factor=row_factors[2], align=True)
    row.prop(property_group, "weight", text="")
    row = row.split(factor=row_factors[3], align=True)

    if total_weight > 0:
        row.label(text=str(round(property_pct*100, 2)))
    else:
        row.label(text="N/A")

    row = row.split(factor=0.03, align=True)
    row.operator(
        RemoveCenterOfMassWeightOperator.bl_idname, text="", icon='TRASH').weight_name = property_group.name



def _create_total_weight_row(column, total_weight, row_factors):
    row = column.split(factor=row_factors[0], align=True)
    row.label(text="")
    row = row.split(factor=row_factors[1], align=True)
    row.label(text="")
    row = row.split(factor=row_factors[2], align=True)

    if total_weight > 0:
        row.label(text="Total: " + str(round(total_weight, 2)))


def _create_buttons(column, collection):
    row = column.row()
    row = column.row()
    row.scale_y = 2
    row = row.split(factor=0.3)

    button_text = "Create default properties"
    if len(collection) > 0:
        button_text = "Reset properties"

    row.operator(
        CreateDefaultCenterOfMassPropertiesOperator.bl_idname, text=button_text)

    # Only show the add button if the base properties has been generated
    if len(collection) > 0:
        row = row.split(factor=0.2)
        row.operator(AddCenterOfMassWeightPropertyOperator.bl_idname, text="Add Property")
