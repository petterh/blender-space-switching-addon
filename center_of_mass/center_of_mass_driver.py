import bpy
from ..properties import addon_preferences
from ..utils import collection_utils
from ..utils import attrib_utils
from ..utils import constraint_utils
from ..utils import blender_utils
from ..utils import driver_utils
from . import center_of_mass_constants as com_constants
from .. import constants
from ..bones.bones import SpaceSwitchTypes


class CenterOfMassRoot():
    def __init__(self, context, armature_object):
        self._armature_object = armature_object
        self.armature = armature_object.data
        self.constraint_driver_paths = []
        self.object = None
        addon_prefs = addon_preferences.get_addon_prefs(context)

        if context.mode == 'OBJECT':
            # Create collection and set the collection to be active
            collection_utils.create_collection(
                context, addon_prefs.collection_name, active=True)

            # Set start frame
            bpy.context.scene.frame_set(context.scene.frame_start)

            self.object = self._create_object(
                context, com_constants.OBJECT_NAME_SUFFIX, armature_object)

            blender_utils.set_active_object(self.object, deselect_all=True)

    def setup_constraints(self):
        blender_utils.set_active_object(self._armature_object)

        weight_collection = attrib_utils.get_attribute(
            self.armature, com_constants.COLLECTION_PROP_NAME)

        total_weight = attrib_utils.get_attribute(
            self.armature, com_constants.TOTAL_WEIGHT_PROP_NAME)

        if len(weight_collection) < 1:
            self.report(
                'WARNING', 'No weight settings exist! Cannot create center of mass driver')
            return

        prop_cnt = 0
        for weight_property_group in weight_collection[:]:
            bone_name = weight_property_group.bone
            if bone_name is None:
                self.report('WARNING', 'No bone name set for ' +
                            weight_property_group.name)
                continue
            weight_pct = weight_property_group.weight / total_weight
            self._create_constraint(
                self._armature_object, bone_name, self.object, weight_pct, prop_cnt)

            prop_cnt += 1

        blender_utils.set_active_object(self.object, deselect_all=True)

    def unselect_driver_object(self):
        self.object.select_set(False)

    def bake_action(self, context):
        """Bake animation to the center of mass root object"""
        # Make sure only this object is selected
        blender_utils.set_active_object(self.object, deselect_all=True)

        # Remove constraint drivers as these will otherwise be left "hanging" will a null reference
        self._remove_constraint_drivers()

        bpy.ops.nla.bake(
            frame_start=context.scene.frame_start,
            frame_end=context.scene.frame_end,
            step=1,
            only_selected=True,
            visual_keying=True,
            clear_constraints=True,
            clear_parents=False,
            clean_curves=True,
            use_current_action=False,
            bake_types={'OBJECT'}
        )

        # Remove unused keys
        old_area_type = bpy.context.area.type
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        bpy.ops.action.clean()
        bpy.context.area.type = old_area_type

        # Rename created action
        self.action_name = self.object.animation_data.action.name = com_constants.ACTION_NAME_PREFIX + \
            self.armature.name

    def _remove_constraint_drivers(self):
        for constraint_driver_path in self.constraint_driver_paths[:]:
            self.object.driver_remove(
                constraint_driver_path)

    def _create_object(self, context, object_name_suffix, driven_armature_object):
        bpy.ops.mesh.primitive_ico_sphere_add(radius=0.25)
        new_object = context.active_object
        new_object.name = driven_armature_object.name + object_name_suffix
        new_object.show_in_front = True
        self._set_properties(
            new_object, driven_armature_object)
        new_object.matrix_world.translation = driven_armature_object.matrix_world.translation

        return new_object

    def _set_properties(self, driver_object, driven_armature_object):
        armature = driven_armature_object.data

        attrib_utils.set_attribute(
            driver_object, com_constants.ARMATURE_REF_PROP_NAME, armature)
        attrib_utils.set_attribute(
            driver_object, constants.DRIVEN_ARMATURE_OBJECT, driven_armature_object)
        attrib_utils.set_attribute(
            armature, com_constants.ROOT_REF_PROP_NAME, driver_object)

    def _create_constraint(self, armature_object, bone_name, driver_object, influence, cnt):
        constraint = constraint_utils.create_location_constraint(
            armature_object, "Center of Mass - " + bone_name, driver_object, bone_name)

        if cnt > 0:
            constraint.influence = influence
            self._create_influence_driver(
                driver_object, armature_object, constraint, cnt)

    def _create_influence_driver(self, driver_object, armature_object, constraint, cnt):
        data_path = 'constraints["' + constraint.name + '"].influence'
        influence_driver = driver_object.driver_add(data_path).driver

        weight_data_path = 'data.' + \
            com_constants.COLLECTION_PROP_NAME + \
            '[' + str(cnt) + '].weight'
        weight_var = self._create_driver_property(
            influence_driver, 'weight', armature_object, weight_data_path)

        total_weight_data_path = 'data.' + \
            com_constants.TOTAL_WEIGHT_PROP_NAME
        total_weight_var = self._create_driver_property(
            influence_driver, 'total_weight', armature_object, total_weight_data_path)

        influence_driver.expression = weight_var.name + \
            ' / ' + total_weight_var.name

        self.constraint_driver_paths.append(data_path)

    def _create_driver_property(self, influence_driver, name, armature_object, data_path):
        driver_var = influence_driver.variables.new()
        driver_var.type = 'SINGLE_PROP'
        driver_var.name = name
        driver_var.targets[0].id = armature_object
        driver_var.targets[0].data_path = data_path
        return driver_var


class CenterOfMassChild():
    def __init__(self, context, center_of_mass_root, armature_object, pose_bone):
        self._armature = armature_object.data
        self._pose_bone = pose_bone
        self._root_object = center_of_mass_root.object

        addon_prefs = addon_preferences.get_addon_prefs(context)
        # Create collection and set the collection to be active
        collection_utils.create_collection(
            context, addon_prefs.collection_name, active=True)

        self._object = driver_utils.create_driver_empty_object(
            context, com_constants.OBJECT_NAME_SUFFIX, armature_object, pose_bone, True)

        # Position object at the driven pose bone position in world space.
        # self._object.matrix_world = armature_object.matrix_world @ pose_bone.matrix
        # self._object.matrix_parent_inverse = self._root_object.matrix_world.inverted()

        # Set center of mass object to be parent
        blender_utils.set_active_object(self._root_object)
        bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)

        constraint_utils.create_location_constraint(
            armature_object, com_constants.CONSTRAINT_NAME_PREFIX + pose_bone.name, self._object, pose_bone.name)

    def bake_and_link_pose_bones(self, context):
        """Bake animation for center of mass child objects and link to the corresponding pose bones"""
        # Make sure only this object is selected
        blender_utils.set_active_object(self._object, deselect_all=True)

        bpy.ops.nla.bake(
            frame_start=context.scene.frame_start,
            frame_end=context.scene.frame_end,
            step=1,
            only_selected=True,
            visual_keying=True,
            clear_constraints=True,
            clear_parents=False,
            clean_curves=True,
            use_current_action=False,
            bake_types={'OBJECT'}
        )

        # Remove unused keys
        old_area_type = bpy.context.area.type
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        bpy.ops.action.clean()
        bpy.context.area.type = old_area_type

        # Rename created action
        self.action_name = self._object.animation_data.action.name = com_constants.ACTION_NAME_PREFIX + \
            self._armature.name

        self._link_to_center_of_mass_driver()

    def _link_to_center_of_mass_driver(self):
        constraint_utils.create_location_constraint(
            self._object, com_constants.CONSTRAINT_NAME, self._pose_bone)
        self._space_switch_type = SpaceSwitchTypes.CENTER_OF_MASS
