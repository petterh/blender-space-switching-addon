import bpy
from .. utils import attrib_utils
from .. utils import prop_utils
from . import center_of_mass_constants as com_constants
# pylint: disable=W0613


def get_groups():
    groups = {
        ('COG', 'COG', 'The COG'),
        ('FEET', 'Feet', 'Belong to the feet set'),
        ('HANDS', 'Hands',
         'Belong to the hands limb set'),
        ('OTHER', 'Other', 'Belong to the other set')}

    return groups


def _get_available_deform_bone_names(self, context):
    armature = context.active_object.data
    yield ('none', 'none', 'none')
    for pose_bone in armature.bones[:]:
        if pose_bone.use_deform:
            yield (pose_bone.name, pose_bone.name, pose_bone.name)

def _update_total_weight(self, context):
    if context.active_object is not None and context.active_object.type == 'ARMATURE':
        armature = context.active_object.data
        collection = attrib_utils.get_attribute(
            armature, com_constants.COLLECTION_PROP_NAME)

        total_weight = 0
        for property_group in collection[:]:
            total_weight += property_group.weight

        attrib_utils.set_attribute(
            armature, com_constants.TOTAL_WEIGHT_PROP_NAME, total_weight)
# pylint: enable=W0613


class CenterOfMassProperty(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Property Name",
                                   description="The name of the property")
    bone: bpy.props.EnumProperty(
        name="Bone",
        description="Selected bone",
        items=_get_available_deform_bone_names)
    weight: bpy.props.FloatProperty(
        name="Weight", min=0, max=100, update=_update_total_weight)


class CenterOfMassChildProperty(bpy.types.PropertyGroup):
    """A bone/controller that can be controlled by a Center of Mass Root"""
    bone_name: bpy.props.StringProperty(name="Bone name",
                                        description="The IK control bone")
    group: bpy.props.StringProperty(name="Property group name",
                                       description="The property group to which these are to belong to")


def register_properties():
    prop_utils.register_collection_property(
        "Center of Mass Properties",
        "Properties driving location for the Center of Mass calculation", CenterOfMassProperty, bpy.types.Armature, com_constants.COLLECTION_PROP_NAME)

    prop_utils.register_collection_property(
        "Center of Mass Children",
        "Bones driven by Center of Mass driver", CenterOfMassChildProperty, bpy.types.Armature, com_constants.CHILD_BONES_PROP_NAME)

    prop_utils.register_float_property("TotaL weight of all children",
                                       "The total weight for all center of mass children in this armature", bpy.types.Armature, com_constants.TOTAL_WEIGHT_PROP_NAME)

    prop_utils.register_pointer_property("Connected armature", "The connected armature driving this Center of Mass object",
                                         bpy.types.Armature, bpy.types.Object, com_constants.ARMATURE_REF_PROP_NAME)

    prop_utils.register_pointer_property("Connected object", "The connected object driven by this armature",
                                         bpy.types.Object, bpy.types.Armature, com_constants.ROOT_REF_PROP_NAME)
