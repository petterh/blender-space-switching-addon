import bpy
from . import center_of_mass_properties
from .center_of_mass_properties import CenterOfMassProperty
from .center_of_mass_properties import CenterOfMassChildProperty

from .center_of_mass_menu import VIEW3D_MT_phspaceswitching_center_of_mass_menu

from .center_of_mass_operators import PHSPACESWITCHING_OT_create_center_of_mass_object
from .center_of_mass_operators import PHSPACESWITCHING_OT_add_to_center_of_mass_child_option

from .center_of_mass_properties_menu import CreateDefaultCenterOfMassPropertiesOperator
from .center_of_mass_properties_menu import AddCenterOfMassWeightPropertyOperator
from .center_of_mass_properties_menu import RemoveCenterOfMassControlledBoneOperator
from .center_of_mass_properties_menu import RemoveCenterOfMassWeightOperator


classes = [
    PHSPACESWITCHING_OT_create_center_of_mass_object,
    PHSPACESWITCHING_OT_add_to_center_of_mass_child_option,
    CreateDefaultCenterOfMassPropertiesOperator,
    AddCenterOfMassWeightPropertyOperator,
    VIEW3D_MT_phspaceswitching_center_of_mass_menu,
    RemoveCenterOfMassControlledBoneOperator,
    RemoveCenterOfMassWeightOperator,
    CenterOfMassProperty,
    CenterOfMassChildProperty
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    center_of_mass_properties.register_properties()


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
