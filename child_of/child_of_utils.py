from .. utils import constraint_utils
from . child_of_constants import CHILD_OF_CONSTRAINT_NAME


def is_child_of_driven(pose_bone):
    return constraint_utils.has_constraint_with_name(pose_bone, CHILD_OF_CONSTRAINT_NAME)
