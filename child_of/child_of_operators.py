import bpy
from .. utils import constraint_utils
from .. import constants
from ..utils import pose_bone_utils as pbone_utils
from . import child_of_constants


# pylint: disable=C0103
class PHSPACESWITCHING_OT_create_child_of_driven_bone(bpy.types.Operator):
    """Create a driver/parent connection between active and selected bones"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".create_child_of_connection"
    bl_label = "Create spaceswitched \"Child Of\" constraint on selected bones"

    use_playback_range: bpy.props.BoolProperty(
        name="Use playback range",
        description="Use the playback range when setting influence",
        default=True
    )

    start_frame: bpy.props.IntProperty(
        name="Start frame",
        description="The start frame where to enable the influence",
        default=0
    )

    def draw(self, context):
        layout = self.layout
        col = layout.column()

        col.prop(self, "use_playback_range")

        if self.use_playback_range is False:
            col.label(text="Select from where to start influence")
            row = col.row()
            row.prop(self, "start_frame")



    @classmethod
    def poll(cls, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            return False

        if context.mode != 'POSE':
            return False

        if len(context.selected_pose_bones) < 2 or context.active_pose_bone is None:
            return False

        if pbone_utils.is_space_switched(context.active_pose_bone):
            return False

        return True

    def execute(self, context):
        armature = context.active_object
        parent_bone = context.active_pose_bone
        pose_bones = context.selected_pose_bones.copy()

        # Select start frame
        if self.use_playback_range:
            selected_start_frame = context.scene.frame_start
        else:
            selected_start_frame = self.start_frame

        for pose_bone in pose_bones[:]:
            if pose_bone.name == parent_bone.name:
                continue

            child_of_constraint = constraint_utils.create_child_of_constraint(
                armature, child_of_constants.CHILD_OF_CONSTRAINT_NAME, pose_bone, parent_bone.name)

            # Set influence keyframes so that the child of constraint does
            # not affect keyframes before the current one.
            self._create_constraint_influence_keyframes(
                child_of_constraint, selected_start_frame)

        return {'FINISHED'}

    def _create_constraint_influence_keyframes(self, constraint, influence_start_frame):
        # Set a key frame for influence on the current frame
        constraint.keyframe_insert(data_path="influence")

        # Set a 0.0 influence on the previous frame,
        # so that the child of is not influencing frames prior to the current one
        constraint.influence = 0.0
        constraint.keyframe_insert(
            data_path="influence", frame=influence_start_frame-1)

    # pylint: disable=W0613
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)
