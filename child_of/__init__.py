import bpy
from .child_of_operators import PHSPACESWITCHING_OT_create_child_of_driven_bone


classes = [
    PHSPACESWITCHING_OT_create_child_of_driven_bone
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
