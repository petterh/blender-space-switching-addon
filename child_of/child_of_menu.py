from .. utils import menu_utils
from .child_of_operators import PHSPACESWITCHING_OT_create_child_of_driven_bone


def create_child_of_menu_items(layout):
    menu_utils.create_menu_option(
        layout, PHSPACESWITCHING_OT_create_child_of_driven_bone)
