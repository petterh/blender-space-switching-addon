import bpy
from ..center_of_mass import center_of_mass_menu
from ..aim_space import aim_space_menu
# from ..fk_ik import fk_ik_menu
from ..world_space import world_space_menu
from ..child_of import child_of_menu
from ..bake import bake_menu
from ..delete import delete_menu

# pylint: disable=C0103
# pylint: disable=W0613


class VIEW3D_MT_phspaceswitching_pose_menu(bpy.types.Menu):
    bl_idname = "VIEW3D_MT_phspaceswitching_pose_menu"
    bl_label = 'Space Switch'

    def draw(self, context):
        layout = self.layout
        world_space_menu.create_aim_space_pose_menu_items(layout)
        aim_space_menu.create_aim_space_pose_menu(layout)
        center_of_mass_menu.create_center_of_mass_menu(layout)
        child_of_menu.create_child_of_menu_items(layout)
        # fk_ik_menu.create_fk_menu_items(layout)
        bake_menu.create_bake_menu_items(layout)
        delete_menu.create_delete_menu_items(layout)


def render_pose_menu(self, context):
    self.layout.menu(
        VIEW3D_MT_phspaceswitching_pose_menu.bl_idname, text="Space Switch")


class VIEW3D_MT_phspaceswitching_object_menu(bpy.types.Menu):
    bl_idname = "VIEW3D_MT_phspaceswitching_object_menu"
    bl_label = 'Space Switch'

    def draw(self, context):
        layout = self.layout
        aim_space_menu.create_aim_space_object_menu(layout)
        center_of_mass_menu.create_center_of_mass_menu(layout)
        bake_menu.create_bake_menu_items(layout)
        delete_menu.create_delete_menu_items(layout)


def render_object_menu(self, context):
    self.layout.menu(
        VIEW3D_MT_phspaceswitching_object_menu.bl_idname, text=VIEW3D_MT_phspaceswitching_object_menu.bl_label)
