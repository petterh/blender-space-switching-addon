import bpy
from . context_menu import VIEW3D_MT_phspaceswitching_object_menu
from . context_menu import VIEW3D_MT_phspaceswitching_pose_menu
from . import context_menu


classes = [
    VIEW3D_MT_phspaceswitching_object_menu,
    VIEW3D_MT_phspaceswitching_pose_menu
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    # pylint: enable=assignment-from-no-return
    bpy.types.VIEW3D_MT_pose_context_menu.append(context_menu.render_pose_menu)
    bpy.types.VIEW3D_MT_object_context_menu.append(
        context_menu.render_object_menu)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    bpy.types.VIEW3D_MT_pose_context_menu.remove(context_menu.render_pose_menu)
    bpy.types.VIEW3D_MT_object_context_menu.remove(
        context_menu.render_object_menu)
