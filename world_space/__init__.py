import bpy
from .world_space_operators import PHSPACESWITCHING_OT_bone_worldspace_driver_object


classes = [
    PHSPACESWITCHING_OT_bone_worldspace_driver_object
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
