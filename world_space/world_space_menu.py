from .. utils import menu_utils
from .world_space_operators import PHSPACESWITCHING_OT_bone_worldspace_driver_object

def create_aim_space_pose_menu_items(layout):
    menu_utils.create_menu_option(
        layout, PHSPACESWITCHING_OT_bone_worldspace_driver_object)