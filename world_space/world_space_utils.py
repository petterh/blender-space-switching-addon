from .. utils import constraint_utils
from . import world_space_constants
def is_worldspace_driven(pose_bone):
    return constraint_utils.has_constraint_with_name(pose_bone, world_space_constants.CONSTRAINT_NAME)
