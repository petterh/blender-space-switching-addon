from .. constants import CONSTRAINT_PREFIX

CONSTRAINT_NAME = CONSTRAINT_PREFIX + 'World Space Constraint'
ACTION_NAME_PREFIX = 'World Space Action for '
