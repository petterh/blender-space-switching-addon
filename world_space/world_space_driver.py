import bpy
from .. utils import constraint_utils
from .. import constants
from . import world_space_constants
from .. utils import collection_utils
from ..properties import addon_preferences
from .. utils import driver_utils


class WorldSpaceDriverObject():
    def __init__(self, context, driven_armature, driven_pose_bone, use_playback_range = False, start_frame = 0, end_frame = 24):

        addon_prefs = addon_preferences.get_addon_prefs(context)
        # Create collection and set the collection to be active
        collection_utils.create_collection(
            context, addon_prefs.collection_name, active=True)

        self.object = driver_utils.create_driver_empty_object(
            context, '-WS', driven_armature, driven_pose_bone, True)

        constraint_utils.create_transform_constraint(driven_armature,
                                                constants.TEMP_CONSTRAINT_NAME,
                                                self.object,
                                                driven_pose_bone.name)

        self._bake_action(context, self.object, driven_pose_bone.name, use_playback_range, start_frame, end_frame)

    def _bake_action(self, context, driver_object, driven_pose_bone_name, use_playback_range, start_frame, end_frame):
        """Bake animation to the driving world space object"""
        if use_playback_range:
            selected_start_frame = context.scene.frame_start
            selected_end_frame = context.scene.frame_end
        else:
            selected_start_frame = start_frame
            selected_end_frame = end_frame

        bpy.ops.nla.bake(
            frame_start=selected_start_frame,
            frame_end=selected_end_frame,
            step=1,
            only_selected=True,
            visual_keying=True,
            clear_constraints=True,
            clear_parents=False,
            clean_curves=True,
            use_current_action=False,
            bake_types={'OBJECT'}
        )

        # Remove unused keys
        old_area_type = bpy.context.area.type
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        bpy.ops.action.clean()
        bpy.context.area.type = old_area_type

        # Rename created action
        self.action_name = driver_object.animation_data.action.name = world_space_constants.ACTION_NAME_PREFIX + \
            driven_pose_bone_name
