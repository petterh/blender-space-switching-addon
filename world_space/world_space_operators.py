import bpy
from . import world_space_driver as driver
from .. import constants
from .. utils import pose_bone_utils as pbone_utils
from .. bones import bones


# pylint: disable=C0103
class PHSPACESWITCHING_OT_bone_worldspace_driver_object(bpy.types.Operator):
    """Create world space driver object for selected bone"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".create_worldspace_object"
    bl_label = "Create worldspace driver"
    bl_options = {'REGISTER', 'UNDO'}

    world_space_switch_type: bpy.props.EnumProperty(
        name='World Space Switch Type',
        items={
            ('COPY_TRANSFORMS', 'transform', 'Transform'),
            ('COPY_ROTATION', 'rotation', 'Rotation'),
            ('COPY_LOCATION', 'location', 'Location')},
        default='COPY_TRANSFORMS')

    use_playback_range: bpy.props.BoolProperty(
        name="Use playback range",
        description="Use the playback range when baking",
        default=True
    )

    start_frame: bpy.props.IntProperty(
        name="Start frame",
        description="The start frame to bake from",
        default=0
    )

    end_frame: bpy.props.IntProperty(
        name="End frame",
        description="Bake action up until this frame",
        default=24
    )

    @classmethod
    def poll(cls, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            return False

        if context.mode != 'POSE':
            return False

        if len(context.selected_pose_bones) < 1 or context.active_pose_bone is None:
            return False

        if pbone_utils.is_space_switched(context.active_pose_bone):
            return False

        return True

    def execute(self, context):
        armature = context.active_object

        pose_bones = context.selected_pose_bones.copy()

        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        for pose_bone in pose_bones[:]:
            space_switched_bone = bones.SpaceSwitchedPoseBone(
                context, self.report, pose_bone)

            # If bone is already connected to a space switching driver object, skip bone
            if space_switched_bone.is_space_switched():
                continue

            world_space_driver = driver.WorldSpaceDriverObject(
                context, armature, pose_bone, self.use_playback_range, self.start_frame, self.end_frame)
            space_switched_bone.link_to_world_space_driver(
                world_space_driver.object, self.world_space_switch_type)

        return {'FINISHED'}

    # pylint: disable=W0613
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()

        col.prop(self, "world_space_switch_type")
        col.prop(self, "use_playback_range")

        if self.use_playback_range is False:
            col.label(text="Select start and end frame")
            row = col.row()
            row.prop(self, "start_frame")
            row.prop(self, "end_frame")
