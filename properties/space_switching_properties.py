import bpy
from .. import constants
from .. utils import prop_utils


def register_properties():

    #
    # Generic Properties
    #
    prop_utils.register_string_property(
        "Space Switched Bone Name", "The pose bone that is space switched (driven).", bpy.types.Object, constants.DRIVEN_BONE_PROPERTY_NAME)

    prop_utils.register_pointer_property(
        "Space Switched Armature Object",
        "The armature where the driven pose bone resides.", bpy.types.Object, bpy.types.Object, constants.DRIVEN_ARMATURE_OBJECT)
