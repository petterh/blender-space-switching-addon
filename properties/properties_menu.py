import bpy
from .. center_of_mass import center_of_mass_properties_menu


class SpaceSwitchingPropertiesPanel(bpy.types.Panel):
    """Space Switching options"""
    bl_label = "Space Switch"
    bl_idname = "PHSPACESWITCHING_PT_properties_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"

    def draw(self, context):
        layout = self.layout

        if context.active_object is not None and context.active_object.type == 'ARMATURE':
            armature = context.active_object.data
            center_of_mass_properties_menu.create_center_of_mass_property_menu(
                armature, layout)
