import bpy
from . addon_preferences import SpaceSwitchingAddOnProperties
from . import space_switching_properties
from . properties_menu import SpaceSwitchingPropertiesPanel

classes = [
    SpaceSwitchingAddOnProperties,
    SpaceSwitchingPropertiesPanel

]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    space_switching_properties.register_properties()

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
