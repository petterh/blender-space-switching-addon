from bpy.types import AddonPreferences
from bpy.props import StringProperty


def get_addon_id():
    addon_id = str(__package__).split('.', maxsplit=1)[0]
    return addon_id


def get_addon_prefs(context):
    preferences = context.preferences
    return preferences.addons[get_addon_id()].preferences


class SpaceSwitchingAddOnProperties(AddonPreferences):
    bl_idname = get_addon_id()

    collection_name: StringProperty(name="Collection Name",
                                    description="The collection name to use when create driver objects etc",
                                    default="Space Switching")

    fk_bone_prefix: StringProperty(name="FK bone prefix",
                                   description="Name prefix for FK bones",
                                   default="FK-")

    ik_bone_prefix: StringProperty(name="IK bone prefix",
                                   description="Name prefix for IK bones",
                                   default="IK-")

    # pylint: disable=W0613
    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'collection_name')
        layout.prop(self, 'fk_bone_prefix')
        layout.prop(self, 'ik_bone_prefix')
