from .. utils import menu_utils
from . operators_fk import PHSPACESWITCHING_OT_bake_ik_to_fk


def create_fk_menu_items(layout):
    menu_utils.create_menu_option(
        layout, PHSPACESWITCHING_OT_bake_ik_to_fk)
