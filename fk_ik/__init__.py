import bpy
from . operators_fk import PHSPACESWITCHING_OT_bake_ik_to_fk


classes = [
    PHSPACESWITCHING_OT_bake_ik_to_fk
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
