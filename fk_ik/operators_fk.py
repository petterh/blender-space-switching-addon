import bpy
from .. utils import constraint_utils
from ..properties import addon_preferences
from .. import constants

# pylint: disable=C0103


class PHSPACESWITCHING_OT_bake_ik_to_fk(bpy.types.Operator):
    """Bake ik bone transforms to the selected fk bones"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".bake_ik_to_fk_bones"
    bl_label = "Bake IK to FK on selected"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        addon_prefs = addon_preferences.get_addon_prefs(context)

        if context.active_object is None:
            return False

        if context.active_object.type == 'ARMATURE':
            if context.mode != 'POSE':
                return False

            if len(context.selected_pose_bones) < 1:
                return False

        return is_fk_bone(addon_prefs, context.active_pose_bone)

    def execute(self, context):
        armature = context.active_object
        pose_bones = context.selected_pose_bones
        addon_prefs = addon_preferences.get_addon_prefs(context)

        self._constrain_valid_fk_bones(armature, addon_prefs, pose_bones)
        self._bake_action(context)

        return {'FINISHED'}

    def _constrain_valid_fk_bones(self, armature, addon_prefs, pose_bones):
        # Go through all selected pose bones
        for pose_bone in pose_bones[:]:
            if is_fk_bone(addon_prefs, pose_bone) is False:
                pose_bone.bone.select = False
                continue

            matching_ik_bone = self._get_matching_ik_bone(
                armature, addon_prefs, pose_bone.name)
            if matching_ik_bone is not None:
                print(matching_ik_bone.name)
                constraint_utils.create_transform_constraint(armature,
                                                        constants.TEMP_CONSTRAINT_NAME,
                                                        pose_bone,
                                                        matching_ik_bone.name)

    def _get_matching_ik_bone(self, armature, addon_prefs, fk_name):
        # Get the matching ik bone name from the supplied fk name
        ik_name = addon_prefs.ik_bone_prefix + \
            fk_name[len(addon_prefs.fk_bone_prefix):]

        for pose_bone in armature.pose.bones[:]:
            if pose_bone.name == ik_name:
                return pose_bone

        return None

    def _bake_action(self, context):
        #bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        # Bake animation to world space object
        bpy.ops.nla.bake(
            frame_start=context.scene.frame_start,
            frame_end=context.scene.frame_end,
            step=1,
            only_selected=True,
            visual_keying=True,
            clear_constraints=True,
            clear_parents=False,
            use_current_action=True,
            bake_types={'POSE'}
        )

        bpy.ops.object.mode_set(mode='POSE', toggle=False)


def is_fk_bone(addon_prefs, pose_bone):
    return pose_bone.name.startswith(addon_prefs.fk_bone_prefix)
