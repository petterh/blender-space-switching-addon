import bpy
from .. import constants
from .. utils import driver_utils
from .. utils import blender_utils

# pylint: disable=C0103
class PHSPACESWITCHING_OT_delete_connected_driver_objects(bpy.types.Operator):
    """Delete all connected driver objects and its actions from selected pose bones"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".delete_connected_drivers"
    bl_label = "Delete connected drivers"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.active_object is None:
            return False

        if context.mode == 'OBJECT':
            if len(driver_utils.get_valid_driver_objects(context.selected_objects)) > 0:
                return True

        if context.mode == 'POSE':
            if len(context.selected_pose_bones) > 0 or context.active_pose_bone is not None:
                return True

        return False

    def execute(self, context):
        initial_mode = context.mode

        if context.mode == 'OBJECT':
            driver_objects = driver_utils.get_valid_driver_objects(
                context.selected_objects)

            # As driver objects are deleted during the process we use names instead.
            driver_object_names = []
            for driver_object in driver_objects[:]:
                driver_object_names.append(driver_object.name)

            driven_armatures = driver_utils.get_armature_objects(
                driver_objects)

            for driven_armature in driven_armatures[:]:
                driven_bone_names = driver_utils.get_related_driven_bones(
                    driven_armature, driver_object_names)

                bpy.ops.object.select_all(action='DESELECT')

                blender_utils.set_active_object(driven_armature)
                bpy.ops.object.mode_set(mode='POSE', toggle=False)
                bpy.ops.pose.select_all(action='DESELECT')

                for driven_bone_name in driven_bone_names[:]:
                    pose_bone = driven_armature.data.bones.get(
                        driven_bone_name)

                    if pose_bone is not None:
                        # A valid bone has been found, enable bake
                        pose_bone.select = True

                pose_bones = context.selected_pose_bones.copy()

                # If any bones are selected trigger delete driver objects
                if len(pose_bones) > 0:
                    driver_utils.delete_driver_objects(context, self.report)

                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        if context.mode == 'POSE':
            #pose_bones = context.selected_pose_bones.copy()
            #armature = context.active_object

            driver_utils.delete_driver_objects(context, self.report)
            bpy.ops.object.mode_set(mode=initial_mode, toggle=False)
        return {'FINISHED'}
