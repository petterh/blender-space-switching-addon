import bpy
from . operators_delete import PHSPACESWITCHING_OT_delete_connected_driver_objects


classes = [
    PHSPACESWITCHING_OT_delete_connected_driver_objects
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
