from .. utils import menu_utils
from . operators_delete import PHSPACESWITCHING_OT_delete_connected_driver_objects


def create_delete_menu_items(layout):
    menu_utils.create_menu_option(
        layout, PHSPACESWITCHING_OT_delete_connected_driver_objects)
