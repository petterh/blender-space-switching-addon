from enum import Enum

from ..aim_space import aim_space_constants
from ..world_space import world_space_constants
from ..center_of_mass import center_of_mass_constants
from ..child_of import child_of_constants

from ..aim_space import aim_space_utils
from ..world_space import world_space_utils
from ..utils import constraint_utils
from ..child_of import child_of_utils
from ..center_of_mass import center_of_mass_utils


class SpaceSwitchTypes(Enum):
    NONE = 0
    WORLD_SPACE = 1
    AIM_SPACE = 2
    CHILD_OF = 3
    CENTER_OF_MASS = 4


# pylint: disable=W0613
class SpaceSwitchedPoseBone():
    def __init__(self, context, report, pose_bone):
        self.pose_bone = pose_bone
        self.name = pose_bone.name
        self._report = report
        self._space_switch_type = self._set_type(pose_bone)

    def clear_constraints(self):
        constraint_utils.remove_constraint_with_name(
            self.pose_bone, aim_space_constants.CONSTRAINT_NAME)
        constraint_utils.remove_constraint_with_name(
            self.pose_bone, world_space_constants.CONSTRAINT_NAME)
        constraint_utils.remove_constraint_with_name(
            self.pose_bone, child_of_constants.CHILD_OF_CONSTRAINT_NAME)
        constraint_utils.remove_constraint_with_name(
            self.pose_bone, center_of_mass_constants.CONSTRAINT_NAME)

    def clear_references(self):
        aim_space_utils.clear_aim_space_reference_properties(self.pose_bone)

    def link_to_world_space_driver(self, driver_object, constraint_type):
        constraint_utils.create_constraint(
            constraint_type, driver_object, world_space_constants.CONSTRAINT_NAME, self.pose_bone)
        self._space_switch_type = SpaceSwitchTypes.WORLD_SPACE

    def link_to_aim_space_driver(self, follow_offset_object, drive_root):
        if drive_root:
            constraint_utils.create_transform_constraint(
                follow_offset_object, aim_space_constants.CONSTRAINT_NAME, self.pose_bone)
        else:
            constraint_utils.create_rotation_constraint(
                follow_offset_object, aim_space_constants.CONSTRAINT_NAME,
                self.pose_bone
            )

        self._space_switch_type = SpaceSwitchTypes.AIM_SPACE

    def is_space_switched(self, report_error=False):
        space_switched = self._space_switch_type != SpaceSwitchTypes.NONE

        self._report({'WARNING'}, 'Pose bone ' + self.name +
                     ' is already space switched!')

        return space_switched

    def _set_type(self, pose_bone):
        is_child_of_driven = child_of_utils.is_child_of_driven(pose_bone)
        is_world_space_driven = world_space_utils.is_worldspace_driven(
            pose_bone)
        is_aim_space_driven = aim_space_utils.is_pose_bone_aimspace_driven(
            pose_bone)
        is_center_of_mass_driven = center_of_mass_utils.is_center_of_mass_driven(
            pose_bone)

        num_drivers = is_aim_space_driven + is_world_space_driven + is_child_of_driven
        if num_drivers > 1:
            self._report({'WARNING'}, 'Pose bone is driven by ' +
                         num_drivers + ' drivers. Only one is allowed!')
            return

        if is_world_space_driven:
            return SpaceSwitchTypes.WORLD_SPACE
        if is_aim_space_driven:
            return SpaceSwitchTypes.AIM_SPACE
        if is_child_of_driven:
            return SpaceSwitchTypes.CHILD_OF
        if is_center_of_mass_driven:
            return SpaceSwitchTypes.CENTER_OF_MASS

        return SpaceSwitchTypes.NONE
