# pylint: disable=C0103
from . import child_of
bl_info = {
    "name": "PH Space Swithing",
    "author": "Petter Hyden",
    "description": "Tools to help with space switching",
    "blender": (3, 4, 1),
    "version": (0, 8, 0),
    "location": "View3D",
    "category": "3D View"
}

# pylint: disable=C0413
from . import center_of_mass
from . import aim_space
from . import world_space
from . import properties
from . import bake
from . import fk_ik
from . import delete
from . import menus


modules = [
    properties,
    menus,
    world_space,
    aim_space,
    center_of_mass,
    child_of,
    fk_ik,
    delete,
    bake
]

def register():
    for module in modules:
        module.register()


def unregister():
    for module in modules:
        module.unregister()


if __name__ == "__main__":
    register()
