# Blender Space Switching Add-on
This is my interpretation of Space Switching for Blender, after taking Richard Lico's wonderful course on the subject.
If you want to learn more about space switching, please take his course. It's awesome! :)

This addon is still in development and should be considered experimental.

CI/CD downloadable/installable addon builds [here](https://gitlab.com/petterh/blender-space-switching-addon/-/releases)

## **General**
All space switching functions are available in the right click context menu of the 3D viewport.
There are some Center of Mass properties to set in the properties panel for the armature,
if you are to use the center of mass functionality.

![Space Switching 3D viewport context menu](/doc/images/space_switch_menu.png)

### **The Space Switching Collection**
All created space switching object/drivers are created in a separate collection. You can change the name of the collection in the add-on preferences.

![Image of space switching collection in the outliner](/doc/images/space_switch_collection.png)

### **Baking**
You have two options when baking the animation back to your driven pose bone.
Either you go the automatic way, where you still have most of the normal action baking options available or you can use the "normal" bake action available in vanilla Blender.

**Automatic / Bake driven pose bone**<br>
The *Bake driven pose bone* menu option will be available when you've selected one or more space switch driver objects, or driven pose bones. You can select all pose bones and it will find all space switched bones and bake them.
It will bake the action and remove the temporary actions, driver objects and constraints in one go.
Basic bake options are available, such as *Clean up, Clean curves, Overwrite Action* and *Range*.

![Bake bone menu option](/doc/images/bake_pose_bone.png)

**Manual baking**<br>
To have full control of the bake action you can use the vanilla Blender *Bake Action* when you have your space switched pose bones selected in *Pose Mode*.

**NOTE**: Temporary space switching objects and its action will not be removed. See **_Delete driver_** below.

### **Delete driver / Cleaning up after space switching**
If you've manually baked the action or you do not wish to bake your space switching, this will remove all temporary driver objects, constraints and actions connected to the selected pose bones, or optionally you can have space switch control objects selected.

![Delete connected driver menu option](/doc/images/delete_driver.png)
<br>
<br>
## **The Different Spaces**
### **World Space**
Take your control (or any pose bone really) bones into world space by adding a world space driver object.

**Create, Edit & Bake**<br>
You create your world space driver, can choose weither to drive location, rotation or the full transform.
You also choose whether to restrict driver to the current playback range or manually define your start and end frame.

![Create world space driver animation](/doc/images/create_worldspace_driver.gif)

You edit, simplify, offset your animation keys and curves for the world space object as needed and then bake them back to the driven pose bone (see **_Baking_** above)
<br>
<br>

### **Aim Space**
In Aim Space we need to create both a *root* and a *target* to have the possibility to drive both the location and the direction in which the bone is aiming towards.
There are two ways to create your aim space drivers

**Create Quick Aim Space Driver**<br>
With this method you setup aim space driver objects with just defining the direction and distance to the aim target object. Keep note of the local direction of your bone and you are fine to go!
You can also choose whether to drive the location of your bone or not.

![Create quick aim space driver animation](/doc/images/create_quick_aim_space_driver.gif)

**Create Template Aim Space Objects**<br>
If you want to have a more exact location for root and target you can start by creating template objects than you can place and then generate the aim space drivers from them. For example, if you want to drive an IK hand holding a tool.
![Create aim space driver from template animation](/doc/images/create_aim_space_driver_from_template.gif)
<br>
<br>

### **Child of**
Does not create a driver object, but fully relies on the child of constraint setting influence at the start of the selected range. Still not fully tested and maybe switch to using the armature constraint instead?

**Example:**<br>
Maybe not the prettiest example, but let's say you want an IK hand + pole target to follow the head? :)
![Create child of relation animation](/doc/images/child_of.gif)

**ToDo**<br>
- Support child of objects (currently only bones are supported)
- Maybe convert from child of constraint to armature constraint?
<br>
<br>

### **Center of Mass (Experimental)**
Still at an experimental level, but works even though the setup up is a bit rough.

**Setup**<br>
To be able to animate based on the world spaced center of mass, we need to define influencing limbs/bones.
Click the _Create default properties_ button in the Space Switch properties panel of your armature to 
generate the default human limbs and there influencing weight for the center of mass.

![Center of mass properties panel](/doc/images/create_default_properties.png)

The default values for a human I got from Richard Lico, which he in turn got them from the Digital Resource Foundation and their
[Virtual Library Project](http://www.oandplibrary.org/).<br>
Direct link to the actual PDF containing data regarding limb weight in relation to full body weight:
http://www.oandplibrary.org/al/pdf/1964_01_044.pdf

![Center of mass default properties panel](/doc/images/default_com_properties.png)

Map your deform bones to the appropriate property to get the correct calculation for the center of mass driver.<br>
You can also tweak, delete, add new properties as needed. The *Name* field is just for your convenience, so that you know what the bone represent.<br>

**Example adding an influencing limb**<br>
Say you wanted to add a tail

![Center of mass default properties panel](/doc/images/center_of_mass_add_property.gif)

**Check and tweak weights**<br>
If you choose the create driver function and untick "*Bake and create limb drivers*", you can check how the center of mass moves and you are able to live tweak the weights of the different limbs/bones and see how this affects the center of mass. Enable motion path for extra clarity.

![Check and tweak values animation](/doc/images/center_of_mass_check_and_tweak.gif)

**Setup center of mass groups**<br>
You need to setup groups of control bones (or any pose bone you wish really) that are optionaly influenced by the center of mass driver object. Normally this would be your COG and any IK and its pole target that can live in their own space. Basically any control that do not follow a long your COG or equivalent control.

![Assign bone to group animation](/doc/images/center_of_mass_assign_bone_to_group.gif)

After that you should have a list similar to this

![List of bones and their group belonging](/doc/images/center_of_mass_groups.png)

**Create & Edit**<br>
Now you are ready to go! Trigger the *create driver* function and have the *Bake and create limb drivers ticked*. 
Select the groups that you want to be affected by the center of mass driver.

**Example**<br>
In this example the arms are in FK and the IK legs are fixed to the ground, so we select only the COG to be affected by the center of mass object/driver.
You are now free to tweak the animation with your center of mass object/driver.

![List of bones and their group belonging](/doc/images/center_of_mass_adjust_animation.gif)
<br>
<br>

### **Spaces still missing in this add-on**

**FK/IK Switching** - Simple IK -> FK transformation copying and vice versa.

