import bpy
from ..utils import prop_utils
from . import aim_space_constants
def register_properties():
    prop_utils.register_pointer_property(
        "Aim Space Root",
        "The object driving the root of the aim space objects", bpy.types.Object, bpy.types.PoseBone, aim_space_constants.ROOT_REF_PROP_NAME)

    prop_utils.register_pointer_property(
        "Aim Space Target",
        "The object driving the aim/target of the two aim space driven objects", bpy.types.Object, bpy.types.PoseBone, aim_space_constants.TARGET_REF_PROP_NAME)
