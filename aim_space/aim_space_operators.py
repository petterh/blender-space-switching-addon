import bpy
from . import aim_space_driver
from .. import constants
from . import aim_space_constants
from .. utils import attrib_utils
from . import aim_space_utils
from .. bones import bones
from ..utils import pose_bone_utils as pbone_utils

# pylint: disable=C0103, W0613


class PHSPACESWITCHING_OT_bone_quick_aimspace_driver_objects(bpy.types.Operator):
    """Create a quick setup aim space driver for selected bone in the selected direction and distance"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".create_quick_aimspace_objects"
    bl_label = "Create quick aim space driver"
    bl_options = {'REGISTER', 'UNDO'}

    aim_space_direction: bpy.props.EnumProperty(
        name='Aim Space Direction',
        items={
            ('x', 'x', 'x'),
            ('y', 'y', 'y'),
            ('z', 'z', 'z'),
            ('-x', '-x', '-x'),
            ('-y', '-y', '-y'),
            ('-z', '-z', '-z')
        },
        default='y')

    distance_to_target: bpy.props.FloatProperty(
        name='Distance to aim target',
        default=1.0
    )

    drive_root: bpy.props.BoolProperty(
        name='Drive bone location (as well as aim)',
        default=False
    )

    use_playback_range: bpy.props.BoolProperty(
        name="Use playback range",
        description="Use the playback range when baking",
        default=True
    )

    start_frame: bpy.props.IntProperty(
        name="Start frame",
        description="The start frame to bake from",
        default=0
    )

    end_frame: bpy.props.IntProperty(
        name="End frame",
        description="Bake action up until this frame",
        default=24
    )

    def draw(self, _):
        layout = self.layout
        col = layout.column()

        col.prop(self, "aim_space_direction")
        col.prop(self, "distance_to_target")
        col.prop(self, "drive_root")
        col.prop(self, "use_playback_range")

        if self.use_playback_range is False:
            col.label(text="Select start and end frame")
            row = col.row()
            row.prop(self, "start_frame")
            row.prop(self, "end_frame")

    @classmethod
    def poll(cls, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            return False

        if context.mode != 'POSE':
            return False

        if len(context.selected_pose_bones) < 1 or context.active_pose_bone is None:
            return False

        # Check if any of the selected bones are not already space switched
        for selected_pose_bone in context.selected_pose_bones[:]:
            if pbone_utils.is_space_switched(selected_pose_bone) is False:
                return True

        return False

    def execute(self, context):
        armature = context.active_object

        if self.use_playback_range:
            selected_start_frame = context.scene.frame_start
            selected_end_frame = context.scene.frame_end
        else:
            selected_start_frame = self.start_frame
            selected_end_frame = self.end_frame

        pose_bones = context.selected_pose_bones.copy()

        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        for pose_bone in pose_bones[:]:
            space_switched_bone = bones.SpaceSwitchedPoseBone(
                context, self.report, pose_bone)
            # If bone is already connected to a space switching driver object, skip bone
            if space_switched_bone.is_space_switched():
                continue

            driver = aim_space_driver.QuickAimSpaceDriver(
                context, armature, pose_bone, self.aim_space_direction, self.distance_to_target, selected_start_frame, selected_end_frame)
            space_switched_bone.link_to_aim_space_driver(
                driver.follow_offset_object, self.drive_root)

        return {'FINISHED'}

    def invoke(self, context, _):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class PHSPACESWITCHING_OT_create_aimspace_template_objects(bpy.types.Operator):
    """Create aim space driver objects to be used as templates for setting up complete aim space drivers"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".create_template_aimspace_objects"
    bl_label = "Create template aim space objects"
    bl_options = {'REGISTER', 'UNDO'}

    aim_space_direction: bpy.props.EnumProperty(
        name='Aim Space Direction',
        items={
            ('x', 'x', 'x'),
            ('y', 'y', 'y'),
            ('z', 'z', 'z'),
            ('-x', '-x', '-x'),
            ('-y', '-y', '-y'),
            ('-z', '-z', '-z')
        },
        default='y')

    distance_to_target: bpy.props.FloatProperty(
        name='Distance to aim target',
        default=1.0
    )


    @classmethod
    def poll(cls, context):
        if context.active_object is None or context.active_object.type != 'ARMATURE':
            return False

        if context.mode != 'POSE':
            return False

        if len(context.selected_pose_bones) < 1 or context.active_pose_bone is None:
            return False

        if pbone_utils.is_space_switched(context.active_pose_bone):
            return False

        return True

    def execute(self, context):
        armature = context.active_object

        pose_bones = context.selected_pose_bones.copy()

        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        for pose_bone in pose_bones[:]:
            aim_space_driver.AimSpaceTemplate(
                context, armature, pose_bone, self.aim_space_direction, self.distance_to_target, context.scene.frame_current)

        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class PHSPACESWITHING_OT_setup_aimspace_from_template_objects(bpy.types.Operator):
    """Setup aim space driver from previously created templates"""
    bl_idname = constants.MODULE_IDNAME_PREFIX + ".setup_aim_space_objects"
    bl_label = "Setup aim space from template"
    bl_options = {'REGISTER', 'UNDO'}

    def _get_driven_pose_bones_with_aim_space_templates(self, context):
        armatures = [
            armature for armature in context.scene.objects if armature.type == 'ARMATURE']

        for armature in armatures[:]:
            for pose_bone in armature.pose.bones[:]:
                if attrib_utils.get_attribute(pose_bone, aim_space_constants.ROOT_REF_PROP_NAME) is not None and \
                        attrib_utils.get_attribute(pose_bone, aim_space_constants.TARGET_REF_PROP_NAME) is not None:
                    yield (pose_bone.name, pose_bone.name, "")

    template: bpy.props.EnumProperty(
        name="Available templates",
        description="Templates previously created",
        items=_get_driven_pose_bones_with_aim_space_templates
    )

    aim_space_direction: bpy.props.EnumProperty(
        name='Aim Space Direction',
        items={
            ('x', 'x', 'x'),
            ('y', 'y', 'y'),
            ('z', 'z', 'z'),
            ('-x', '-x', '-x'),
            ('-y', '-y', '-y'),
            ('-z', '-z', '-z')
        },
        default='z')

    drive_root: bpy.props.BoolProperty(
        name='Drive bone location (as well as aim)',
        default=False
    )
    use_playback_range: bpy.props.BoolProperty(
        name="Use playback range",
        description="Use the playback range when baking",
        default=True
    )

    start_frame: bpy.props.IntProperty(
        name="Start frame",
        description="The start frame to bake from",
        default=0
    )

    end_frame: bpy.props.IntProperty(
        name="End frame",
        description="Bake action up until this frame",
        default=24
    )

    def draw(self, _):
        layout = self.layout
        col = layout.column()

        col.prop(self, "aim_space_direction")
        col.prop(self, "distance_to_target")
        col.prop(self, "drive_root")
        col.prop(self, "use_playback_range")

        if self.use_playback_range is False:
            col.label(text="Select start and end frame")
            row = col.row()
            row.prop(self, "start_frame")
            row.prop(self, "end_frame")

    @classmethod
    def poll(cls, context):

        if context.mode != 'OBJECT':
            return False

        if aim_space_utils.aim_space_templates_available(context):
            return True

        return False

    def execute(self, context):
        if self.use_playback_range:
            selected_start_frame = context.scene.frame_start
            selected_end_frame = context.scene.frame_end
        else:
            selected_start_frame = self.start_frame
            selected_end_frame = self.end_frame

        result = self._get_pose_bone_and_armature(context, self.template)
        pose_bone = result[1]
        armature = result[0]

        if pose_bone is None:
            self.report({'WARNING'}, 'Cannot find pose bone ' + self.template)
            return {'CANCELLED'}

        if armature is None:
            self.report(
                {'WARNING'}, 'Cannot find armature for pose bone ' + self.template)
            return {'CANCELLED'}

        space_switched_bone = bones.SpaceSwitchedPoseBone(
            context, self.report, pose_bone)

        aim_space_root = attrib_utils.get_attribute(
            pose_bone, aim_space_constants.ROOT_REF_PROP_NAME)
        aim_space_target = attrib_utils.get_attribute(
            pose_bone, aim_space_constants.TARGET_REF_PROP_NAME)

        driver = aim_space_driver.SetupAimSpaceFromTemplate(
            context, armature, pose_bone, aim_space_root, aim_space_target, self.aim_space_direction, selected_start_frame, selected_end_frame)
        space_switched_bone.link_to_aim_space_driver(
            driver.follow_offset_object, self.drive_root)

        return {'FINISHED'}

    def invoke(self, context, _):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def _get_pose_bone_and_armature(self, context, pose_bone_name):
        armatures = [
            armature for armature in context.scene.objects if armature.type == 'ARMATURE']

        for armature in armatures[:]:
            for pose_bone in armature.pose.bones[:]:
                if pose_bone.name == pose_bone_name:
                    return armature, pose_bone
        return None, None
