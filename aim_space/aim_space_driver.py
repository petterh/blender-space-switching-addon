import bpy
import mathutils
from . import aim_space_constants
from ..utils import constraint_utils
from ..utils import collection_utils
from . import aim_space_utils
from ..properties import addon_preferences
from ..utils import driver_utils


class AimSpaceTemplate():
    """Create the two objects/empties that can be used as a base for setting up the complete aim space driver objects"""

    def __init__(self, context, driven_armature_object, driven_pose_bone, target_direction, target_distance, start_frame):

        self._context = context
        self._driven_pose_bone = driven_pose_bone

        addon_prefs = addon_preferences.get_addon_prefs(context)

        # Create collection and set the collection to be active
        collection = collection_utils.create_collection(
            context, addon_prefs.collection_name)
        collection_utils.set_collection_active(collection.name)

        context.scene.frame_current = start_frame

        self.root_object = self._create_root_object(
            driven_armature_object, driven_pose_bone)
        self.target_object = self._create_target_object(
            driven_armature_object, driven_pose_bone)

        # Position root
        self.root_object.matrix_world = driven_armature_object.matrix_world @ driven_pose_bone.matrix

        # Position target
        self.target_object.matrix_world = self.root_object.matrix_world
        self._set_target_location(target_direction, target_distance)

        # Setup reference properties
        aim_space_utils.set_aim_space_reference_properties(
            driven_pose_bone, self.root_object, self.target_object)

    def _create_root_object(self, driven_armature, driven_pose_bone):
        return driver_utils.create_driver_empty_object(self._context, '-AS_ROOT', driven_armature, driven_pose_bone, True)

    def _create_target_object(self, driven_armature, driven_pose_bone):
        return driver_utils.create_driver_empty_object(self._context, '-AS_TARGET', driven_armature, driven_pose_bone, True)

    def _set_target_location(self, target_direction, target_distance):
        """Based on the enum string for target_direction we set the direction of the target position"""
        if target_direction == 'x':
            vec = mathutils.Vector((target_distance, 0.0, 0.0))
        elif target_direction == '-x':
            vec = mathutils.Vector((-target_distance, 0.0, 0.0))
        if target_direction == 'y':
            vec = mathutils.Vector((0.0, target_distance, 0.0))
        elif target_direction == '-y':
            vec = mathutils.Vector((0.0, -target_distance, 0.0))
        if target_direction == 'z':
            vec = mathutils.Vector((0.0, 0.0, target_distance))
        elif target_direction == '-z':
            vec = mathutils.Vector((0.0, 0.0, -target_distance))

        inv = self.target_object.matrix_world.copy()
        inv.invert()

        # vec aligned to local axis in Blender 2.8+
        # in previous versions: vec_rot = vec * inv
        vec_rot = vec @ inv
        self.target_object.location = self.target_object.location + vec_rot


class SetupAimSpaceFromTemplate():
    """Setup aim space driver from template objects"""

    def __init__(self, context, driven_armature, driven_pose_bone, root_object, target_object, track_axis, start_frame, end_frame):

        self._driven_pose_bone = driven_pose_bone

        addon_prefs = addon_preferences.get_addon_prefs(context)

        # Create collection and set the collection to be active
        collection = collection_utils.create_collection(
            context, addon_prefs.collection_name)
        collection_utils.set_collection_active(collection.name)

        context.scene.frame_current = start_frame

        self.root_object = root_object
        self.target_object = target_object
        self.offset_object = self._create_offset_object(
            context, driven_armature, driven_pose_bone, track_axis)
        self.follow_offset_object = self._create_follow_offset_object(context,
            driven_armature, driven_pose_bone)

        self._create_driver_action(context,
            self.root_object, driven_armature, driven_pose_bone.name, start_frame, end_frame)
        self._create_driver_action(context,
            self.target_object, driven_armature, driven_pose_bone.name, start_frame, end_frame)

        # Hide offset objects
        self.follow_offset_object.hide_viewport = True
        self.offset_object.hide_viewport = True

    def _create_follow_offset_object(self, context, driven_armature, driven_pose_bone):
        """Create the follow object to be driving the driven pose bone"""
        follow_offset_object = driver_utils.create_driver_empty_object(
            context, '-AS_FOLLOW_OFFSET', driven_armature, driven_pose_bone)
        follow_offset_object.matrix_world = driven_armature.matrix_world @ driven_pose_bone.matrix

        # Parent
        self._parent(context, self.offset_object, follow_offset_object)

        return follow_offset_object

    def _create_offset_object(self, context, driven_armature, driven_pose_bone, selected_track_axis):
        """Create offset object to compensate for rotation differences between aim driver object and driven pose bone"""
        offset_object = driver_utils.create_driver_empty_object(
            context, '-AS_TARGET_OFFSET', driven_armature, driven_pose_bone)
        offset_object.matrix_world = self.root_object.matrix_world
        constraint_utils.create_damped_track_constraint(self.target_object, '-AS_TARGET_TRACK', offset_object,
                                                   None, track_axis=selected_track_axis)
        bpy.ops.object.visual_transform_apply()

        # Parent
        self._parent(context, self.root_object, offset_object)

        return offset_object

    def _parent(self, context, parent_object, child_object):
        # Parent
        bpy.ops.object.select_all(action='DESELECT')
        parent_object.select_set(True)
        child_object.select_set(True)
        context.view_layer.objects.active = parent_object
        bpy.ops.object.parent_set(keep_transform=True)

    def _create_driver_action(self, context, driver_object, driven_armature, driven_pose_bone_name, bake_start_frame, bake_end_frame):
        bpy.ops.object.select_all(action='DESELECT')
        driver_object.select_set(True)
        context.view_layer.objects.active = driver_object

        child_of_constraint = constraint_utils.create_child_of_constraint(
            driven_armature, aim_space_constants.CHILD_OF_CONSTRAINT_NAME, driver_object, driven_pose_bone_name)

        # Set a key frame for influence on the current frame
        child_of_constraint.influence = 1.0
        child_of_constraint.keyframe_insert(
            data_path="influence", frame=bake_start_frame)
        child_of_constraint.influence = 0.0
        child_of_constraint.keyframe_insert(
            data_path="influence", frame=bake_start_frame-1)

        temp_action = driver_object.animation_data.action
        bpy.ops.nla.bake(
            frame_start=bake_start_frame,
            frame_end=bake_end_frame,
            step=1,
            only_selected=True,
            visual_keying=True,
            clear_constraints=True,
            clear_parents=False,
            use_current_action=False,
            clean_curves=True,
            bake_types={'OBJECT'}
        )

        # Remove unused keys
        old_area_type = bpy.context.area.type
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        bpy.ops.action.clean()
        bpy.context.area.type = old_area_type

        # Delete unused temp action after bake
        bpy.data.actions.remove(temp_action)

        driver_object.animation_data.action.name = aim_space_constants.ACTION_NAME_PREFIX + \
            driven_pose_bone_name


class QuickAimSpaceDriver():
    """Create a quick setup of aim space driver with no option but to select direction and distance to target"""

    def __init__(self, context, driven_armature, driven_pose_bone, target_direction, target_distance, start_frame, end_frame):
        self._context = context

        self._driven_pose_bone = driven_pose_bone

        # Setup base aim space objects via aim space template
        aim_space_template = AimSpaceTemplate(
            context, driven_armature, driven_pose_bone, target_direction, target_distance, start_frame)
        driver = SetupAimSpaceFromTemplate(context, driven_armature, driven_pose_bone,
                                           aim_space_template.root_object, aim_space_template.target_object, target_direction, start_frame, end_frame)
        self.follow_offset_object = driver.follow_offset_object
