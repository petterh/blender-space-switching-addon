import bpy
from . import aim_space_properties

from . aim_space_menu import VIEW3D_MT_phspaceswitching_aim_space_object_menu
from . aim_space_menu import VIEW3D_MT_phspaceswitching_aim_space_pose_menu

from . aim_space_operators import PHSPACESWITCHING_OT_bone_quick_aimspace_driver_objects
from . aim_space_operators import PHSPACESWITCHING_OT_create_aimspace_template_objects
from . aim_space_operators import PHSPACESWITHING_OT_setup_aimspace_from_template_objects

classes = [
    VIEW3D_MT_phspaceswitching_aim_space_object_menu,
    VIEW3D_MT_phspaceswitching_aim_space_pose_menu,
    PHSPACESWITCHING_OT_bone_quick_aimspace_driver_objects,
    PHSPACESWITCHING_OT_create_aimspace_template_objects,
    PHSPACESWITHING_OT_setup_aimspace_from_template_objects
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    aim_space_properties.register_properties()


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
