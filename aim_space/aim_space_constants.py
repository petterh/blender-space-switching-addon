from .. constants import CONSTRAINT_PREFIX

DRIVER_NAME = 'Aim Space Driver'
CONSTRAINT_NAME = CONSTRAINT_PREFIX + 'Aim Space Constraint'
CHILD_OF_CONSTRAINT_NAME = 'Aim Space: Child of'
ACTION_NAME_PREFIX = 'Aim Space Action for '
ROOT_REF_PROP_NAME = 'phss_aim_space_root'
TARGET_REF_PROP_NAME = 'phss_aim_space_target'
