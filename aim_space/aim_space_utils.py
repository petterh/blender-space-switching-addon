from ..utils import constraint_utils
from ..utils import attrib_utils
from . import aim_space_constants

def is_pose_bone_aimspace_driven(pose_bone):
    return constraint_utils.has_constraint_with_name(pose_bone, aim_space_constants.CONSTRAINT_NAME)


def set_aim_space_reference_properties(pose_bone, root_object, target_object):
    # Setup reference properties
    attrib_utils.set_attribute(
        pose_bone, aim_space_constants.ROOT_REF_PROP_NAME, root_object)
    attrib_utils.set_attribute(
        pose_bone, aim_space_constants.TARGET_REF_PROP_NAME, target_object)


def clear_aim_space_reference_properties(pose_bone):
    attrib_utils.clear_attribute_if_it_exists(
        pose_bone, aim_space_constants.ROOT_REF_PROP_NAME)
    attrib_utils.clear_attribute_if_it_exists(
        pose_bone, aim_space_constants.TARGET_REF_PROP_NAME)


def aim_space_templates_available(context):
    armatures = [
        armature for armature in context.scene.objects if armature.type == 'ARMATURE']

    for armature in armatures[:]:
        for pose_bone in armature.pose.bones[:]:
            if attrib_utils.get_attribute(pose_bone, aim_space_constants.ROOT_REF_PROP_NAME) is not None and \
                    attrib_utils.get_attribute(pose_bone, aim_space_constants.TARGET_REF_PROP_NAME) is not None:
                return True

    return False
