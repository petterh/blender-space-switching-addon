import bpy
from ..utils import menu_utils
from . import aim_space_operators

# pylint: disable=C0103


class VIEW3D_MT_phspaceswitching_aim_space_object_menu(bpy.types.Menu):
    bl_idname = "VIEW3D_MT_phspaceswitching_aim_space_object_menu"
    bl_label = 'Aim Space'

    # pylint: disable=W0613
    def draw(self, context):
        layout = self.layout
        self._create_aim_space_object_menu_items(layout)


    def _create_aim_space_object_menu_items(self, layout):
        menu_utils.create_menu_option(
        layout, aim_space_operators.PHSPACESWITHING_OT_setup_aimspace_from_template_objects)


class VIEW3D_MT_phspaceswitching_aim_space_pose_menu(bpy.types.Menu):
    bl_idname = "VIEW3D_MT_phspaceswitching_aim_space_pose_menu"
    bl_label = 'Aim Space'

    # pylint: disable=W0613
    def draw(self, context):
        layout = self.layout
        self._create_aim_space_pose_menu_items(layout)

    def _create_aim_space_pose_menu_items(self, layout):
        menu_utils.create_menu_option(
            layout, aim_space_operators.PHSPACESWITCHING_OT_bone_quick_aimspace_driver_objects)

        menu_utils.create_menu_option(
            layout, aim_space_operators.PHSPACESWITCHING_OT_create_aimspace_template_objects)


def create_aim_space_pose_menu(layout):
    layout.menu(VIEW3D_MT_phspaceswitching_aim_space_pose_menu.bl_idname, text=VIEW3D_MT_phspaceswitching_aim_space_pose_menu.bl_label)


def create_aim_space_object_menu(layout):
    layout.menu(VIEW3D_MT_phspaceswitching_aim_space_object_menu.bl_idname,
                text=VIEW3D_MT_phspaceswitching_aim_space_object_menu.bl_label)
